<html>

<head>
	<title> EveryKet - 모두의 마켓 </title>
	<meta charset = "utf-8">

	<script>

	var selected=0;

	//Guest Tab selection

	function guest() {
           var element = document.getElementById('guest');
           element.style.opacity = "0.5";
           selected=0;
           select();
    }

    //Shopkeeper Tab selection

    function shopkeeper() {
           var element = document.getElementById('shopkeeper');
           element.style.opacity = "0.5";
           selected=1;
           select();
    }

    function select(){

    	if (selected==1) {
    		document.getElementById('guest').style.opacity = "1";
    		document.getElementById('form_login').action = "shopkeeper_main.php"
    	}
    	else {
    		document.getElementById('shopkeeper').style.opacity = "1";
    		document.getElementById('form_login').action = "guest_main.php"
    	}
    }


	</script>

	<style>

	body {margin:0px; color:#5A5A5A;}

	#Top {width:100%; height:200px; background-color:#FDFFFF;}
	#guest {width:50%; height:100%; background-color:#CEFBC9; float:left; float:bottom; z-index:-1; opacity:1;}
	#shopkeeper {width:50%; height:100%; background-color:#D4F4FA; float:right; float:bottom; z-index:-1; opacity:1;}
	#login {margin-left:40%; margin-right:40%; height:100px; width:300px; position:absolute; margin-top:300px; background-color:#FDFFFF;}

	</style>

</head>

<body>

	<div id="Top"> EveryKet </div>
	<div id="guest" onclick="guest()"> guest</div>
	<div id="shopkeeper" onclick="shopkeeper()"> shopkeeper </div>

	 <div id="login"> 
		<form method="POST" id="form_login" action="guest_main.php">

			<fieldset>

			Id (mail address) <input type="text" id="id" length="30"> <br> 
			Password <input type="password" id="pw" length="30"> 

			</fieldset>

		<div align="center"> <a href="signup.php"> Don't you have account for EveryKet? </a> <br>

		<button type="submit"> Sign in </button> </div>

		</form>
	 </div> 

<div id="map" style="width:100%;height:350px;"></div>
<p><em>지도를 클릭해주세요!</em></p> 
<div id="clickLatlng"></div>

<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=390f039564634614e868366e385092c5&libraries=services"></script>


<script>
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };

var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

// 지도를 클릭한 위치에 표출할 마커입니다
var marker = new daum.maps.Marker({ 
    // 지도 중심좌표에 마커를 생성합니다 
    position: map.getCenter() 
}); 
// 지도에 마커를 표시합니다
marker.setMap(map);

// 지도에 클릭 이벤트를 등록합니다
// 지도를 클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
daum.maps.event.addListener(map, 'click', function(mouseEvent) {        
    
    // 클릭한 위도, 경도 정보를 가져옵니다 
    var latlng = mouseEvent.latLng; 
    
    // 마커 위치를 클릭한 위치로 옮깁니다
    marker.setPosition(latlng);
    
    var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
    message += '경도는 ' + latlng.getLng() + ' 입니다';
    
    var resultDiv = document.getElementById('clickLatlng'); 
    resultDiv.innerHTML = message;
    
});
</script>



</body>


</html>