<html>
<head>
	<meta charset="utf-8">
</head>

<body>

	<form name="reg_shop" method="POST" action="add_shop_db.php" onsubmit="val_alloc()">

		<table name="tb" border="0">

		<tr>
		<td> SHOP NAME </td> <td> <input type="text" length="30" name="shop_name"> <input type="button" value="check" onClick="chk_name();"> (DO NOT USE SPACE IN SHOP NAME) </td> </tr>

		<tr>
		<td> Shop Address by "ADDRESS" </td> <td>	
		<input type="text" id="sample5_address" name="shop_address" placeholder="주소">
		<input type="button" onclick="sample5_execDaumPostcode()" value="주소 검색"><br>
		<div id="map" style="width:300px;height:300px;margin-top:10px;display:none"></div>

		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		<script src="//apis.daum.net/maps/maps3.js?apikey=390f039564634614e868366e385092c5&libraries=services"></script>
		<script>

		function chk_input() {
			if (reg_shop.shop_name.value=="") {
				alert("input your shop name");
				reg_shop.shop_name.focus();
				return false;
			}
			else if (reg_shop.shop_address.value=="") {
				alert("input your shop address");
				reg_shop.shop_address.focus();
				return false;
			}
			else if (reg_shop.code.value=="") {
				alert("input your enter code"); 
				reg_shop.code.focus();
				return false;
			}
			else if (reg_shop.code_con.value=="") {
				alert("input enter code again"); 
				reg_shop.code_con.focus();
				return false;
			}
			else if (reg_shop.code.value != reg_shop.code_con.value) {
				alert("values of password and password again should be the same");
				return false;
			}
			else return true;
		}

		function chk_name() {
			window.open('shopname_chk.php?shop_name='+reg_shop.shop_name.value, 'Shop Name Check', 'width=400, height=200');
		}


    	var mapContainer = document.getElementById('map'), // 지도를 표시할 div
        	mapOption = {
            center: new daum.maps.LatLng(37.537187, 127.005476), // 지도의 중심좌표
            level: 5 // 지도의 확대 레벨
        };

    //지도를 미리 생성
    	var map = new daum.maps.Map(mapContainer, mapOption);
    //주소-좌표 변환 객체를 생성
    	var geocoder = new daum.maps.services.Geocoder();
    //마커를 미리 생성
    	var marker = new daum.maps.Marker({
        position: new daum.maps.LatLng(37.537187, 127.005476),
        map: map
    });

    	function sample5_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = data.address; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 기본 주소가 도로명 타입일때 조합한다.
                if(data.addressType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 주소 정보를 해당 필드에 넣는다.
                document.getElementById("sample5_address").value = fullAddr;
                // 주소로 좌표를 검색
                geocoder.addr2coord(data.address, function(status, result) {
                    // 정상적으로 검색이 완료됐으면
                    if (status === daum.maps.services.Status.OK) {
                        // 해당 주소에 대한 좌표를 받아서
                        var coords = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);
                        // 지도를 보여준다.
                        mapContainer.style.display = "block";
                        map.relayout();
                        // 지도 중심을 변경한다.
                        map.setCenter(coords);
                        // 마커를 결과값으로 받은 위치로 옮긴다.
                        marker.setPosition(coords)

                    }
                });
            }
        }).open();
    }
	</script> </td> </tr>

 <!--********************************************************************************클릭한 위치에 마커 표시******************-->

	<tr><td> Shop Address by "COORD" </td> 
	<td>	
	
<div id="map2" style="width:100%;height:350px;"></div>
<p><em>지도를 클릭해주세요! 그래야 정확한 주소가 등록된답니다! </em></p> 
<div id="clickLatlng"></div>

<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=390f039564634614e868366e385092c5"></script>


<script>
var mapContainer = document.getElementById('map2'), // 지도를 표시할 div 
    mapOption = { 
        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };

var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

// 지도를 클릭한 위치에 표출할 마커입니다
var marker = new daum.maps.Marker({ 
    // 지도 중심좌표에 마커를 생성합니다 
    position: map.getCenter() 
}); 
// 지도에 마커를 표시합니다
marker.setMap(map);

// 지도에 클릭 이벤트를 등록합니다
// 지도를 클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
daum.maps.event.addListener(map, 'click', function(mouseEvent) {        
    
    // 클릭한 위도, 경도 정보를 가져옵니다 
    var latlng = mouseEvent.latLng; 
    
    // 마커 위치를 클릭한 위치로 옮깁니다
    marker.setPosition(latlng);
    
    var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
    message += '경도는 ' + latlng.getLng() + ' 입니다';
    
    var resultDiv = document.getElementById('clickLatlng'); 
    resultDiv.innerHTML = message;

    window.lati1 = latlng.getLat();
    window.lngi1 = latlng.getLng();
    
});
</script>
</td> </tr>
	    
			<tr>
			<td> Telephone Number </td> <td> <input type="number" name="tel1" length="3"> - <input type="number" name="tel2" length="4"> - <input type="number" name="tel3" length="4"> <td>
			</tr>

			<tr>
			<td> Shop details </td> <td> <textarea cols="150" rows="5" name="shop_detail" placeholder="가게 소개말을 입력하세요"></textarea> </td> </tr>

			<tr>
			<td> Shop Structure </td> <td> 

		<!-- Canvas Start -->

		<div id="canvas" width="100%" border="1px">

			<script>

		window.count_counter = 1;
		window.count_socket = 1;
		window.count_tb2 = 1;
		window.count_tb4 = 1;
		window.count_tb6 = 1;
		window.count_toilet = 1;
		window.count_wifi = 1;

		window.left_off = [];
		window.left_off[0] = ['start',0];
		window.off_count = 1;


		function onDragStart(event) {
			if (event.target.tagName.toLowerCase() == 'img') {
				event.dataTransfer.setData("Text", event.target.id);
			}
			else {
				event.preventDefault();
			}
		}

		function on_DragEnter(event) {
			var types = event.dataTransfer.types;
			for (var i=0; i<types.length; i++) {
				if (types[i] == "Text") {
					event.preventDefault();
				}
			}
		}

		function on_DragOver(event) {
			event.dataTransfer.dropEffect = "copy";
			event.preventDefault();
		}

		function on_Drop(event) {
			var id = event.dataTransfer.getData("Text");
			var drop = document.getElementById(id);

			if (drop && drop.parentNode != event.currentTarget && id == 'counter') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_counter.png";
				clone.id = 'counter'+window.count_counter;
				clone.name = 'counter'+window.count_counter;
				clone.style.width = "30px";
				clone.style.height = "45px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('counter'+window.count_counter));

				document.getElementById('count_counter').value = window.count_counter;
				window.left_off[window.off_count] = ['counter'+window.count_counter,window.left_off[window.off_count-1][1]+30];
				window.off_count += 1;

				window.count_counter += 1;

			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'socket') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_socket.png";
				clone.id = 'socket'+window.count_socket;
				clone.name = 'socket'+window.count_socket;
				clone.style.width = "20px";
				clone.style.height = "20px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('socket'+window.count_socket));

				document.getElementById('count_socket').value = window.count_socket;
				window.left_off[window.off_count] = ['socket'+window.count_socket,window.left_off[window.off_count-1][1]+20];
				window.off_count += 1;

				window.count_socket += 1;
			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'tb2') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_tb2.png";
				clone.id = 'tb2_'+window.count_tb2;
				clone.name = 'tb2_'+window.count_tb2;
				clone.style.width = "40px";
				clone.style.height = "30px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('tb2_'+window.count_tb2));

				document.getElementById('count_tb2').value = window.count_tb2;
				window.left_off[window.off_count] = ['tb2_'+window.count_tb2,window.left_off[window.off_count-1][1]+40];
				window.off_count += 1;

				window.count_tb2 += 1;
			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'tb4') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_tb4.png";
				clone.id = 'tb4_'+window.count_tb4;
				clone.name = 'tb4_'+window.count_tb4;
				clone.style.width = "50px";
				clone.style.height = "40px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('tb4_'+window.count_tb4));

				document.getElementById('count_tb4').value = window.count_tb4;
				window.left_off[window.off_count] = ['tb4_'+window.count_tb4,window.left_off[window.off_count-1][1]+50];
				window.off_count += 1;

				window.count_tb4 += 1;
			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'tb6') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_tb6.png";
				clone.id = 'tb6_'+window.count_tb6;
				clone.name = 'tb6_'+window.count_tb6;
				clone.style.width = "60px";
				clone.style.height = "50px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('tb6_'+window.count_tb6));

				document.getElementById('count_tb6').value = window.count_tb6;
				window.left_off[window.off_count] = ['tb6_'+window.count_tb6,window.left_off[window.off_count-1][1]+60];
				window.off_count += 1;

				window.count_tb6 += 1;
			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'toilet') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_toilet.png";
				clone.id = 'toilet'+window.count_toilet;
				clone.name = 'toilet'+window.count_toilet;
				clone.style.width = "20px";
				clone.style.height = "20px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('toilet'+window.count_toilet));

				document.getElementById('count_toilet').value = window.count_toilet;
				window.left_off[window.off_count] = ['toilet'+window.count_toilet,window.left_off[window.off_count-1][1]+20];
				window.off_count += 1;

				window.count_toilet += 1;
			}

			else if (drop && drop.parentNode != event.currentTarget && id == 'wifi') {
				var clone = document.createElement('img');
				clone.src = "http://www.everyket.com/image/stru_wifi.png";
				clone.id = 'wifi'+window.count_wifi;
				clone.name = 'wifi'+window.count_wifi;
				clone.style.width = "20px";
				clone.style.height = "20px";
				clone.style.position = "relative";
				document.getElementById('div1').appendChild(clone);
				Drag.init(document.getElementById('wifi'+window.count_wifi));

				document.getElementById('count_wifi').value = window.count_wifi;
				window.left_off[window.off_count] = ['wifi'+window.count_wifi,window.left_off[window.off_count-1][1]+20];
				window.off_count += 1;

				window.count_wifi += 1;
			}


			else if ( id.search('counter') && id != 'counter' ) {
			}

			else if ( id.search('socket') && id != 'socket' ) {
			}

			else if ( id.search('tb2_') && id != 'tb2_' ) {
			}

			else if ( id.search('tb4_') && id != 'tb4_' ) {
			}

			else if ( id.search('tb6_') && id != 'tb6_' ) {
			}

			else if ( id.search('toilet') && id != 'toilet' ) {
			}

			else if ( id.search('wifi') && id != 'wifi' ) {
			}

			event.stopPropagation();
		}

		function findPosX(obj) {
		    var curleft = 0;
		    if (obj.offsetParent)
		        while (true) {
		            curleft += obj.offsetLeft;
		            if(!obj.offsetParent) break;
		            obj = obj.offsetParent;
		        }
		    else if (obj.x)
		        curleft += obj.x;
		    return curleft;
		}
 
		function findPosY(obj) {
		    var curtop = 0;
		    if (obj.offsetParent)
		        while (true) {
		            curtop += obj.offsetTop;
		            if (!obj.offsetParent) break;
		            obj = obj.offsetParent;
		        }
		    else if (obj.y)
		        curtop += obj.y;
		    return curtop;
		}

		var Drag = {

			obj : null,

			init : function(o, oRoot, minX, maxX, minY, maxY, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper)
			{
				o.onmousedown	= Drag.start;

				o.hmode			= bSwapHorzRef ? false : true ;
				o.vmode			= bSwapVertRef ? false : true ;

				o.root = oRoot && oRoot != null ? oRoot : o ;

				if (o.hmode  && isNaN(parseInt(o.root.style.left  ))) o.root.style.left   = "0px";
				if (o.vmode  && isNaN(parseInt(o.root.style.top   ))) o.root.style.top    = "0px";
				if (!o.hmode && isNaN(parseInt(o.root.style.right ))) o.root.style.right  = "0px";
				if (!o.vmode && isNaN(parseInt(o.root.style.bottom))) o.root.style.bottom = "0px";

				o.minX	= typeof minX != 'undefined' ? minX : null;
				o.minY	= typeof minY != 'undefined' ? minY : null;
				o.maxX	= typeof maxX != 'undefined' ? maxX : null;
				o.maxY	= typeof maxY != 'undefined' ? maxY : null;

				o.xMapper = fXMapper ? fXMapper : null;
				o.yMapper = fYMapper ? fYMapper : null;

				o.root.onDragStart	= new Function();
				o.root.onDragEnd	= new Function();
				o.root.onDrag		= new Function();
			},

			start : function(e)
			{
				var o = Drag.obj = this;
				e = Drag.fixE(e);
				var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
				var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
				o.root.onDragStart(x, y);

				o.lastMouseX	= e.clientX;
				o.lastMouseY	= e.clientY;

				if (o.hmode) {
					if (o.minX != null)	o.minMouseX	= e.clientX - x + o.minX;
					if (o.maxX != null)	o.maxMouseX	= o.minMouseX + o.maxX - o.minX;
				} else {
					if (o.minX != null) o.maxMouseX = -o.minX + e.clientX + x;
					if (o.maxX != null) o.minMouseX = -o.maxX + e.clientX + x;
				}

				if (o.vmode) {
					if (o.minY != null)	o.minMouseY	= e.clientY - y + o.minY;
					if (o.maxY != null)	o.maxMouseY	= o.minMouseY + o.maxY - o.minY;
				} else {
					if (o.minY != null) o.maxMouseY = -o.minY + e.clientY + y;
					if (o.maxY != null) o.minMouseY = -o.maxY + e.clientY + y;
				}

				document.onmousemove	= Drag.drag;
				document.onmouseup		= Drag.end;

				return false;
			},

			drag : function(e)
			{
				e = Drag.fixE(e);
				var o = Drag.obj;

				var ey	= e.clientY;
				var ex	= e.clientX;
				var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
				var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
				var nx, ny;

				if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
				if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
				if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
				if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

				nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
				ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

				if (o.xMapper)		nx = o.xMapper(y)
				else if (o.yMapper)	ny = o.yMapper(x)

				Drag.obj.root.style[o.hmode ? "left" : "right"] = nx + "px";
				Drag.obj.root.style[o.vmode ? "top" : "bottom"] = ny + "px";
				Drag.obj.lastMouseX	= ex;
				Drag.obj.lastMouseY	= ey;

				Drag.obj.root.onDrag(nx, ny);
				return false;
			},

			end : function()
			{
				document.onmousemove = null;
				document.onmouseup   = null;
				Drag.obj.root.onDragEnd(	parseInt(Drag.obj.root.style[Drag.obj.hmode ? "left" : "right"]), 
											parseInt(Drag.obj.root.style[Drag.obj.vmode ? "top" : "bottom"]));
				Drag.obj = null;
			},

			fixE : function(e)
			{
				if (typeof e == 'undefined') e = window.event;
				if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
				if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
				return e;
			}
		};


			</script>	

			<div id="div1" style="width:79%; height:600px; display:inline-block; border:1px solid black; float:left;"
				onDrop="on_Drop(event)" onDragOver="on_DragOver(event)" onDragEnter="on_DragEnter(event)">


			</div>

			<div id="div2" style="width:20%; height:600px; border:1px solid black; display:inline-block; float:right;">

				<img src="http://www.everyket.com/image/stru_counter.png" id="counter" width="45px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_socket.png" id="socket" width="45px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_tb2.png" id="tb2" width="45px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_tb4.png" id="tb4" width="60px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_tb6.png" id="tb6" width="75px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_toilet.png" id="toilet" width="45px" height="45px" onDragStart="onDragStart(event)" draggable="true">
				<img src="http://www.everyket.com/image/stru_wifi.png" id="wifi" width="45px" height="45px" onDragStart="onDragStart(event)" draggable="true">

			</div>

		</div>

		</td>
		</tr>

		<tr>
			<td> Enter Code </td>
			<td> <input type="text" name="code" length="4"><br> Please enter 4-digit numbers. </td>
		</tr>
		<tr>
			<td> Code confirm </td>
			<td> <input type="text" name="code_con" length="4"> </td>
		</tr>

		<tr>
			<td colspan="2" align="center"> <button type="submit"> submit </button> <button type="reset"> reset </button>

		</table>

			<br><br><br>

			

			<input type="hidden" name="latitude" id="latitude">
			<input type="hidden" name="longitude" id="longitude">

			<input type="hidden" name="count_counter" id="count_counter" value="0">
			<input type="hidden" name="count_socket" id="count_socket" value="0">
			<input type="hidden" name="count_tb2" id="count_tb2" value="0">
			<input type="hidden" name="count_tb4" id="count_tb4" value="0">
			<input type="hidden" name="count_tb6" id="count_tb6" value="0">
			<input type="hidden" name="count_toilet" id="count_toilet" value="0">
			<input type="hidden" name="count_wifi" id="count_wifi" value="0">

			<div id="dynamic_loc">

			</div>

			<script>

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"counter'+i+'_x\" id=\"counter'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"counter'+i+'_y\" id=\"counter'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"socket'+i+'_x\" id=\"socket'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"socket'+i+'_y\" id=\"socket'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb2_'+i+'_x\" id=\"tb2_'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb2_'+i+'_y\" id=\"tb2_'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb4_'+i+'_x\" id=\"tb4_'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb4_'+i+'_y\" id=\"tb4_'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb6_'+i+'_x\" id=\"tb6_'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"tb6_'+i+'_y\" id=\"tb6_'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"toilet'+i+'_x\" id=\"toilet'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"toilet'+i+'_y\" id=\"toilet'+i+'_y\">';

			}

			for (i=1; i<=10; i++) {

				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"wifi'+i+'_x\" id=\"wifi'+i+'_x\">';
				document.getElementById('dynamic_loc').innerHTML += '<input type=\"hidden\" name=\"wifi'+i+'_y\" id=\"wifi'+i+'_y\">';

			}


			</script>

		</form>

		<script>

		function val_alloc() {

			var lati = window.lati1;
			document.getElementById('latitude').value = lati;

			var lngi = window.lngi1;
			document.getElementById('longitude').value = lngi;

			window.new_left = [];

			for(i=1; i<=window.off_count-1; i++){

				var origin_left = document.getElementById(window.left_off[i][0]).style.left;
				var origin_num = Number(origin_left.replace("px",""));
				var new_num = origin_num + window.left_off[i-1][1];
				window.new_left[i] = new_num+'px';

			}


			for(i=1; i<=(window.count_counter-1); i++){

				document.getElementById('counter'+i+'_x').value = document.getElementById('counter'+i).style.left;
				document.getElementById('counter'+i+'_y').value = document.getElementById('counter'+i).style.top;
			}

			for(i=1; i<=(window.count_socket-1); i++){

				document.getElementById('socket'+i+'_x').value = document.getElementById('socket'+i).style.left;
				document.getElementById('socket'+i+'_y').value = document.getElementById('socket'+i).style.top;
			}

			for(i=1; i<=(window.count_tb2-1); i++){

				document.getElementById('tb2_'+i+'_x').value = document.getElementById('tb2_'+i).style.left;
				document.getElementById('tb2_'+i+'_y').value = document.getElementById('tb2_'+i).style.top;
			}

			for(i=1; i<=(window.count_tb4-1); i++){

				document.getElementById('tb4_'+i+'_x').value = document.getElementById('tb4_'+i).style.left;
				document.getElementById('tb4_'+i+'_y').value = document.getElementById('tb4_'+i).style.top;
			}

			for(i=1; i<=(window.count_tb6-1); i++){

				document.getElementById('tb6_'+i+'_x').value = document.getElementById('tb6_'+i).style.left;
				document.getElementById('tb6_'+i+'_y').value = document.getElementById('tb6_'+i).style.top;
			}

			for(i=1; i<=(window.count_toilet-1); i++){

				document.getElementById('toilet'+i+'_x').value = document.getElementById('toilet'+i).style.left;
				document.getElementById('toilet'+i+'_y').value = document.getElementById('toilet'+i).style.top;
			}

			for(i=1; i<=(window.count_wifi-1); i++){

				document.getElementById('wifi'+i+'_x').value = document.getElementById('wifi'+i).style.left;
				document.getElementById('wifi'+i+'_y').value = document.getElementById('wifi'+i).style.top;
			}

			for (i=1; i<= window.left_off.length; i++) {

				document.getElementById(window.left_off[i][0]+'_x').value= window.new_left[i];
			}

		};

		</script>

</body>
</html>