<div class="col-sm-3">
					<ul class="nav nav-pills nav-stacked well">
						<li>
							<a href="pos_index.php">Index</a>
						</li>
                        
                        
                        <?php if ($_SESSION['shopid'] != "") {?>
                        
                        <li  <?php if($_SESSION['curpage']=="table_index"){echo "class = 'active'";} ?> >
							<a href="table_index.php">Table Management</a>
						</li>
						<li  <?php if($_SESSION['curpage']=="inventory"){echo "class = 'active'";} ?> >
							<a href="inventory.php">Inventory</a>
						</li>
                        <li  <?php if($_SESSION['curpage']=="inventory_list"){echo "class = 'active'";} ?> >
							<a href="inventory_list.php">Inventory List</a>
						</li>
                        <li  <?php if($_SESSION['curpage']=="stock"){echo "class = 'active'";} ?> >
							<a href="stock.php">Stock</a>
						</li>
                        
                        <li  <?php if($_SESSION['curpage']=="stock_shopwise"){echo "class = 'active'";} ?> >
							<a href="stock_shopwise.php">Stock List</a>
						</li>
						
						<li  <?php if($_SESSION['curpage']=="transaction"){echo "class = 'active'";} ?> >
							<a href="transaction.php">Daily Transactions</a>
						</li>
						<li  <?php if($_SESSION['curpage']=="transaction_hist"){echo "class = 'active'";} ?> >
							<a href="transaction_hist.php">Transactions Shop-wise</a>
						</li>
						
						
						</br>
						<li <?php if($_SESSION['curpage']=="logout"){echo "class = 'active'";} ?> >
							<a href="logout.php">LogOut</a>
						</li>
                        
                        
                        <?php  } else { ?>
                        <li  <?php if($_SESSION['curpage']=="login"){echo "class = 'active'";} ?> >
							<a href="login.php">Login</a>
						</li>
                        
                        <?php } ?>
					</ul>
				</div>