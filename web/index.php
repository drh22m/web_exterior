<html>

<head>
	<title> EveryKet - 모두의 마켓 </title>
	<meta charset = "utf-8">
	<link rel="shortcut icon" type="image/x-icon" href="http://www.everyket.com/favicon.ico" />

		
	<script>

	var selected=0;

	//Guest Tab selection

	function guest() {
           var element = document.getElementById('guest');
           element.style.opacity = "0.5";
           selected=0;
           select();
    }

    //Shopkeeper Tab selection

    function shopkeeper() {
           var element = document.getElementById('shopkeeper');
           element.style.opacity = "0.5";
           selected=1;
           select();
    }

    function select(){

    	if (selected==1) {
    		document.getElementById('guest').style.opacity = "1";
    		document.getElementById('form_login').action = "shopkeeper_main.php"
    	}
    	else {
    		document.getElementById('shopkeeper').style.opacity = "1";
    		document.getElementById('form_login').action = "guestmain.php"
    	}
    }
    
    
    function chk_loginform() {
		if (form_login.mail_add.value=="") {
			alert('Enter your email address!');
			form_login.mail_add.focus();
			return false;
		}
		else if (form_login.pw.value=="") {
			alert('Enter your password!');
			form_login.pw.focus();
			return false;
		}
		else return true;
	}


	</script>

	<style>

	body {margin:0px; color:#5A5A5A;}

	#Top {width:100%; height:220px; background-color:#FDFFFF;}
	#guest {width:50%; height:100%; background-color:#FDFFFF; float:left; float:bottom; z-index:-1; opacity:1;}
	#shopkeeper {width:50%; height:100%; background-color:#FDFFFF; float:right; float:bottom; z-index:-1; opacity:1;}
	#login {margin-left:40%; margin-right:40%; height:100px; width:300px; position:absolute; margin-top:300px; background-color:#FDFFFF;}

	</style>

</head>

<body>

	<div id="Top" align="center"> <a href="http://www.everyket.com"> <img src="http://www.everyket.com/image/logo_everyket.png"> </a> </div>
	<div id="guest" align="left" onclick="guest()"> <img src="http://www.everyket.com/image/logo_guest.png"> </div>
	<div id="shopkeeper" align="right" onclick="shopkeeper()"> <img src="http://www.everyket.com/image/logo_shopkeeper.png"> </div>

	 <div id="login" align="center" style="margin-left:36%; margin-top:15%;"> 
		<form method="POST" id="form_login" onSubmit="return chk_loginform();" action="guestmain.php">
			<fieldset>
			<table>
			<tr>
			<td align="center"> <img src="http://www.everyket.com/image/index_id.png"> </td> <td> <input type="text" name="mail_add" length="30"> </td>
			</tr>
			<tr>
			<td align="center"> <img src="http://www.everyket.com/image/index_pw.png"> </td> <td> <input type="password" name="pw" length="30"> </td>
			</tr>
			<tr>
				<td colspan="2" align="center"> <a href="signup.php"> <img src="http://www.everyket.com/image/index_signup.png"></a> </td>
			</tr>
			<tr>
				<td colspan="2" align="center"> <button type="submit"> <img src="http://www.everyket.com/image/index_signin.png"> </button> </td>
			</tr>

			</table>
			</fieldset>		

		</form>
	 </div> 


</body>

</html>

