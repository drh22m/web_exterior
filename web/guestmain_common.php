<!DOCTYPE html>
<html>
<head>
	<title> EveryKet </title>
	<meta charset="utf-8"/>
	<link rel = "stylesheet" type="text/css" />

	<style>

		#acc {
			background-image : url('image/bg2.png');
			background-repeat: no-repeat;
			background-size: 100%;
		}

		#body {
			background-color: white;
			color: #6C6C6C;
			padding:0px;
			margin:0px;
			text-align: center;
		}
	
		#header {
			    background-color:black;
			    color: #6C6C6C;
			    text-align:center;
			    padding:0px;
		}
		#nav {
				color: #6C6C6C;
			    line-height:30px;
			    background-color:#eeeeee;
			    height:800px;
			    width:40%;
			    float:left;
			    padding:0px;
			}

		#section {
				color: #6C6C6C;
			    width: 60%;
			    float:left;
			    padding:0px; 
			}
		#report{ 
				width:100%; 
				height:50px;
				float:right;
				text-align:right;
				color: #6C6C6C;
		}

		a {
			color:#008299; 
		}
		a:hover {
			color:#393939;
		}

		button {

			border: 1px solid #b5def5;
			background-color: #EEEEEE;
		}

	</style>
	
</head>

<body>

<?php

			$connect = mysql_connect('localhost', 'root', '2rjsqlalf!');
			mysql_select_db('shoplist', $connect);

			$sql = "SELECT * from location";
			$result = mysql_query($sql, $connect);
			$count_field = mysql_num_fields($result);
			$count_rows = mysql_num_rows($result);

			$loca = array();

			for ($a = 0; $a < $count_rows; $a ++ ){

				for ($b = 0; $b < $count_field; $b ++ ) {

					$loca[$a][$b] = mysql_result($result, $a, $b);
				}
			}

?>

<!-- // 로고 부분 // -->

	<div style="background-color:white; color:#6C6C6C; align:center;">

	<p align="center">
	<a href="http://www.everyket.com"> <img src="http://www.everyket.com/image/logo_everyket.png" height="100px"> </a> </p>

	</div>

<!--map -->
 <script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=390f039564634614e868366e385092c5"></script>
 <script>
//user current location 

		window.onload = function(){
	  
		  	if(navigator.geolocation) {
		   		navigator.geolocation.getCurrentPosition(MyPosition);   
		  	}
		}

		var loca_arr = <?php echo json_encode($loca) ?>;

		var row_for_loop = <?=$count_rows?>;

		for (var a=0; a<row_for_loop; a++){
			loca_arr[a][1] = Number(loca_arr[a][1]);
			loca_arr[a][2] = Number(loca_arr[a][2]);
		}

		function MyPosition(position){

			var count_row = <?=$count_rows?>;
			var count_field = <?=$count_field?>;

			var lat = position.coords.latitude;
			var lng = position.coords.longitude;

			for (var a=0; a<row_for_loop; a++) {

				loca_arr[a][3] = distance(lat,lng,loca_arr[a][1],loca_arr[a][2]);
			}

			loca_arr.sort(compareFourthColumn);

			function compareFourthColumn(a, b) {
			    if (a[3] === b[3]) {
			        return 0;
			    }
			    else {
			        return (a[3] < b[3]) ? -1 : 1;
			    }
			}

			for (var x=0; x<count_row; x++){

					document.getElementById('table_sec').innerHTML += '<a href=\"shopinfo.php?shop_name='+loca_arr[x][0]+'\" target=\"_blank\"><h4>'+loca_arr[x][0]+'</h4></a> ( '+loca_arr[x][3]+' km from you )';

			}

			function info(i) {
				window.open('shopinfo.php?shopname='+i, 'info', 'width=800, height=600');
			}


			var container = document.getElementById('nav');

			var options = {
				center: new daum.maps.LatLng(lat, lng),
				level: 3
			};

			var map = new daum.maps.Map(container, options);

										
			var positions = [];

			positions[0] = { title : 'current', latlng : new daum.maps.LatLng( lat, lng ) };


			for (var j = 1; j <= count_row; j++) {

				positions[j] = { title : loca_arr[(j-1)][0] , latlng : new daum.maps.LatLng( loca_arr[(j-1)][1] , loca_arr[(j-1)][2] ) };

			}



					// 마커 이미지의 이미지 주소입니다
					var imageSrc = "http://www.everyket.com/image/marker.png"; 
					    
					for (var i = 0; i < positions.length; i ++) {
					    
					    // 마커 이미지의 이미지 크기 입니다
					    var imageSize = new daum.maps.Size(40, 50); 
					    
					    // 마커 이미지를 생성합니다    
					    var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize); 
					    
					    // 마커를 생성합니다
					    var marker = new daum.maps.Marker({
					        map: map, // 마커를 표시할 지도
					        position: positions[i].latlng, // 마커를 표시할 위치
					        title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
					        image : markerImage // 마커 이미지 
					    });
					}

	 	}


 		function pw_chk() {
		
			var password = prompt("Enter your password again to edit your account ");
			
			var ses_pw = "<?=$ses_password?>" ;
			
				if(ses_pw==password) {
					return true;
					}
				else {
					alert('Wrong password..try again!');
					return false;
				}
		}

		function distance(lat1, lon1, lat2, lon2, unit) {

		    var radlat1 = Math.PI * lat1/180;
		    var radlat2 = Math.PI * lat2/180;
		    var radlon1 = Math.PI * lon1/180;
		    var radlon2 = Math.PI * lon2/180;
		    var theta = lon1-lon2;
		    var radtheta = Math.PI * theta/180;
		    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

		    dist = Math.acos(dist);
		    dist = dist * 180/Math.PI;
		    dist = dist * 60 * 1.1515;
		    if (unit=="K") { dist = dist * 1.609344 }
		    if (unit=="N") { dist = dist * 0.8684 }

		    return dist;
		}


</script>

<div style="width:100%; height:100px; color: #6C6C6C; background-color:white; padding:0px; margin:0px; float:right; text-align:right;" id="acc">

	 	<table border="0px" align="left" style="width:30%; height:100px; float:right;">
	 		<tr>
	 			<td> <form action="my_account.php" method="post" onclick="return pw_chk()"> <button type="submit"> my account </button> </form> </td>
	 			<td> <form action="shopkeepermain_fromguest.php" mothod="POST"> <button type="submit" > Shopkeeper </button> </form> </td>
	 			<td> HI <?php echo "{$ses_name}"; ?> </td>
	 			<td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
	 		</tr>
	 	</table>
</div>

<div id="nav" >
	<input type="hidden" id="latitude">
	<input type="hidden" id="longitude">
</div>


<div id="section">
	<div id="explanation" style="padding:10px;">
	<h1> &nbsp;&nbsp;Guest Main </h1>
	<h5> &nbsp;&nbsp;- following shops are sorted by distance from your current location - </h5>
	<h5> &nbsp;&nbsp;- click shop's name to see the detail - </h5>
	</div>

<div id="table_sec" style="margin:10px; padding:10px;" link="#1F50B5" vlink="#393939"></div>


</div>

<div id="report">
&nbsp; &nbsp; Contact Us : <a href="mailto:drh22m@gmail.com? Subject=Hello%20Drh22m" target="_top"> E-mail </a>

</div>

</body>
</html>



<!--
													echo "latitude , longitude";

													$connect = mysql_connect('localhost', 'root', '2rjsqlalf!');
													mysql_select_db('shoplist', $connect);

													$sql = "select latitude, longitude from shoplist";
													$result = mysql_query($sql, $connect);
													$count = mysql_num_fields($result);

													echo " 서버 접속 {$result} 성공 ";

													?>

													
													while($rows=mysql_fetch_row($result))
													{
														echo "<tr>";
														for($a=0; $a <$count; $a++)
														{
															echo "<td> $rows[$a] </td>";

														}

														echo "</tr>";
													}
														?>

-->